package self.Que28.java;
import java.util.Scanner;

public class StringReverse {
	public static void main (String args []) {	
		
		String reverse = "" , word ;
		
		Scanner scan = new Scanner(System.in);
		System.out.print("Enter an input String: " );
		word =scan.nextLine();
		
		int wordLength = word.length();
		int j = wordLength-1;
		
		for (int i =j; i>=0; i-- ) {
			reverse = reverse+  word.charAt(i);
		}
		System.out.println("The reverse of Computer-Programming is " + reverse);
	}
		
	}
/*
 output
 
 Enter an input String: Computer-Programming
The reverse of Computer-Programming is gnimmargorP-retupmoC
 */



