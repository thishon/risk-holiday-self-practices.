package self.Que14.java;

public class DoubleForLoop {
public static void main (String args []) {
		
		int row = 10;
		for (int i = 1; i <= row; i = i+2) {
			
			for (int j = 1; j <= row-1; j++) {
				
				if (j >= i) {
					System.out.print( j );
				}
			}
			System.out.println(" ");
		}
		
		
	}

}


