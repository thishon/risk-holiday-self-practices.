package self.Que14.java;

import java.util.Scanner;

public class IsRightAngleTriangle {
	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		System.out.print("SIDE 1 : ");
		double side1 = scan.nextInt();
		System.out.print("SIDE 2 : ");
		double side2 = scan.nextInt();
		System.out.print("SIDE 3 : ");
		double side3 = scan.nextInt();
		System.out.print("IS RIGHT ANGLE TRIANGLE : " + ValidTriangle(side1, side2, side3));
	}

	public static boolean ValidTriangle(double side1, double side2, double side3) {
		return (Math.pow(side1, 2)+Math.pow(side2, 2) == Math.pow(side3, 2)) || 
				(Math.pow(side1, 2)+Math.pow(side3, 2) == Math.pow(side2, 2)) ||
				(Math.pow(side2, 2)+Math.pow(side3, 2) == Math.pow(side1, 2));
	}
}


