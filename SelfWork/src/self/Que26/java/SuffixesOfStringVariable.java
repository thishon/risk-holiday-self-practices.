package self.Que26.java;

public class SuffixesOfStringVariable {

	public static void main(String[] args) {
		String word = "hurricanes";
		
		int wordLength = word.length();
		
		for (int line = 0; line <= wordLength; line++) {
			for (int i = line; i <= line; i++) {
				System.out.print(word.substring(i));
			}
			System.out.println(" ");
}
	}
}

/*
output

hurricanes 
urricanes 
rricanes 
ricanes 
icanes 
canes 
anes 
nes 
es 
s 
 

*/