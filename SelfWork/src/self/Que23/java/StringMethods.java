package self.Que23.java;

public class StringMethods {
	public static void main(String[] args) {
		String word = "School.of.Progressive. Rock";
		String pat = "in";
	
			System.out.println(word.length());
			System.out.println(word.substring(22));
			System.out.println(word.substring(22, 24));
			System.out.println(word.indexOf("oo"));
			System.out.println(word.toUpperCase());
			System.out.println(word.lastIndexOf("o"));
			System.out.println(word.indexOf("ok"));

	}
}

/*

OUTPUT

27
Rock
R
3
SCHOOL.OF.PROGRESSIVE. ROCK
24
-1

															*/