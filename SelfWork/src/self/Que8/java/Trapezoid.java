package self.Que8.java;

public class Trapezoid {
	public static void main(String args[]) {
		double top = 6;
		double bottom = 12;
		double height = 8;
		double Area = (bottom + top) * height / 2;
		System.out.print("TOP \t: ");
		System.out.println(top);
		System.out.print("BOTTOM \t: ");
		System.out.println(bottom);
		System.out.print("HEIGHT \t: ");
		System.out.println(height);
		System.out.print("AREA \t: ");
		System.out.println(Area);
	}
}